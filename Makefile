#
# This file is part of liab-studio-android.
#
# Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# liab-studio-android is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# liab-studio-android is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
#

GRADLE=gradle
FIRST_GCLFAGS=--daemon --parallel --exclude-task lintVitalRelease
GFLAGS=$(FIRST_GCLFAGS) --no-rebuild --offline

all: FORCE
	$(GRADLE) $(GFLAGS)  assemble installDebug

build: FORCE
	$(GRADLE) $(GFLAGS) assemble

install: FORCE
	$(GRADLE) $(GFLAGS) installDebug

init:
	$(GRADLE) $(FIRST_GCLFAGS) assemble

FORCE:
