#
# This file is part of liab-studio-android.
# Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# liab-studio-android is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# liab-studio-android is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

define all-c-cpp-files-under
	$(patsubst ./%, %, 													\
		$(shell cd $(LOCAL_PATH) ; 										\
			find $(1) -name "*.c" -or -name "*.cpp")					\
		)
endef

LOCAL_SRC_FILES:= $(call all-c-cpp-files-under, .)

LOCAL_C_INCLUDES:= $(LOCAL_PATH)/header $(LOCAL_PATH)

# configurations
APP_ABI:= all
LOCAL_LDLIBS:= -llog -lGLESv2
LOCAL_CPP_FEATURES:= exceptions
LOCAL_MODULE:= liab-studio
LOCAL_SHARED_LIBRARIES:= libcsv muparser jbox0 box0 usb-1.0
LOCAL_SHARED_LIBRARIES += libreplot freetype2 fontconfig harfbuzz expat png

ifeq ($(NDK_DEBUG),"1")
# debug build
LOCAL_CFLAGS += -g -Wall -Wwrite-strings -Wsign-compare
LOCAL_CFLAGS += -Wextra  -Wno-empty-body -Wno-unused-parameter
LOCAL_CFLAGS += -Wformat-security
endif

include $(BUILD_SHARED_LIBRARY)

import-it = 										\
	$(call import-add-path,$(shell dirname $(1)));	\
	$(call import-module,$(shell basename $(1))/$(2))

$(call import-it,$(LIBUSB_PATH),android/jni)
$(call import-it,$(LIBBOX0_PATH),android/jni)
$(call import-it,$(JBOX0_PATH),src/main/jni)
$(call import-it,$(MUPARSER_PATH),build/android/jni)
$(call import-it,$(FREETYPE2_PATH),builds/android/jni)
$(call import-it,$(LIBREPLOT_PATH),android/jni)
$(call import-it,$(LIBCSV_PATH),android/jni)
$(call import-it,$(LIBPNG_PATH),android/jni)
$(call import-it,$(HARFBUZZ_PATH),android/jni)
