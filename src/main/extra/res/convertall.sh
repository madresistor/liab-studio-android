#/bin/sh
SVG2PNG=$PWD/svg2png.sh

$SVG2PNG -w48 ic_launcher.svg
$SVG2PNG -w48 ic_action_column_add.svg
$SVG2PNG -w48 ic_action_curve_add.svg
$SVG2PNG -w96 ic_graph.svg
$SVG2PNG -w96 ic_box0.svg
$SVG2PNG -w96 ic_recent.svg
$SVG2PNG -w96 ic_table.svg
