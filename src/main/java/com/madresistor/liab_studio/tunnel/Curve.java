/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.tunnel;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.JniLoader;

import java.util.LinkedList;

public class Curve {
	private static final String TAG = Column.class.getName();

	/**
	 * Append a Curve
	 * @param x X Axis column
	 * @param y Y Axis column
	 * @throws RuntimeException
	 */
	public static native int appendCurve(int x, int y) throws RuntimeException;

	/**
	 * remove Curve
	 * @param c curve number
	 */
	public static native void removeCurve(int c);
	public static native void removeCurveAll();

	/**
	 * Get the total number of curves.
	 * @return count
	 */
	public static native int getCurveCount();

	/**
	 * Get the curve name
	 * @param c curve number
	 * @return name
	 */
	public static native String getName(int c);

	/**
	 * Set the curve name
	 * @param c curve number
	 * @param value String name
	 */
	public static native void setName(int c, String value);

	/**
	 * Get X axis column
	 * @param c curve number
	 * @return X axis column number
	 */
	public static native int getColumnX(int c);

	/**
	 * Set X axis column
	 * @param c curve number
	 * @param x X column number
	 */
	public static native void setColumnX(int c, int x);

	/**
	 * Get Y axis column
	 * @param c curve number
	 * @return Y axis column number
	 */
	public static native int getColumnY(int c);

	/**
	 * Set Y axis column
	 * @param c curve number
	 * @param y Y column number
	 */
	public static native void setColumnY(int c, int y);

	/**
	 * Set the curve color
	 * @param c curve number
	 * @param color Color embedded in 32bit (8:8:8:8 RGBA)
	 */
	public static native void setColor(int c, int color);

	/**
	 * get the curve color
	 * @param c curve number
	 * @return color embedded in 32bit value (8:8:8:8 RGBA)
	 */
	public static native int getColor(int c);

	/**
	 * Set the line width
	 * @param c curve number
	 * @param width Width
	 */
	public static native void setLineWidth(int c, float width);

	/**
	 * Get the line width
	 * @param c curve number
	 * @return curve width
	 */
	public static native float getLineWidth(int c);

	/**
	 * Set if the curve is shown to user
	 * @param c curve number
	 * @param visible Visibility
	 */
	public static native void setVisible(int c, boolean visible);

	/**
	 * Get if the curve is visible to user
	 * @param c curve number
	 * @param true if visible
	 */
	public static native boolean isVisible(int c);

	private final static Handler m_handler = new Handler(Looper.getMainLooper());
	private final static Runnable m_notifier = new Runnable() {
		public void run() {
			if (Config.INFO) Log.i(TAG, "changed");

			for (Observer obs: m_observers) {
				obs.onCurveChanged();
			}
		}
	};

	/**
	 * Send a onCurveChanged() callback to all observer
	 */
	public static void changed() {
		m_handler.post(m_notifier);
	}

	/**
	 * Register a observer.
	 * A observer will be notified when anything in curve is changed.
	 * callback will be performed on main loop
	 * @param obs Observer
	 */
	public static boolean registerObserver(Observer obs) {
		return m_observers.add(obs);
	}

	/**
	 * Unregister a observer that was previously register with registerObserver()
	 * @param obs Observer
	 */
	public static boolean unregisterObserver(Observer obs) {
		return m_observers.remove(obs);
	}

	/* afaif, LinkedList should work better than ArrayList */
	private static final LinkedList<Observer> m_observers
		= new LinkedList<Observer>();

	public interface Observer {
		public void onCurveChanged();
	}

	static {
		JniLoader.glue();
	}
}
