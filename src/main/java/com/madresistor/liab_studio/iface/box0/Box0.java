/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.madresistor.box0.Device;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.Iface;
import com.madresistor.liab_studio.iface.box0.DeviceManager;
import com.madresistor.liab_studio.iface.box0.DurationPickerDialog;
import com.madresistor.liab_studio.iface.box0.NothingToStartDialog;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.iface.box0.source.Source;
import com.madresistor.liab_studio.iface.box0.source.ain.AinSource;
import com.madresistor.liab_studio.iface.box0.source.i2c.I2cSource;
import com.madresistor.liab_studio.iface.box0.source.spi.SpiSource;
import com.madresistor.liab_studio.tunnel.Column;
import com.madresistor.liab_studio.widget.table.column.RemoveAllDialog;

public class Box0 extends Iface
		implements DeviceManager.OnStatusChangedListener {
	private final static String TAG = Box0.class.getName();
	private final DeviceManager m_deviceManager;
	private final Source m_ain, m_spi, m_i2c;

	private final SharedPreferences m_prefs;
	public final static String
		PREFS_NAME = "Box0PrefsFile",
		PREF_WARN_BEFORE_CLEARING_TABLE = "warn",
		PREF_AUTO_SWITCH_GRAPH = "auto_switch_graph",
		PREF_AUTO_STOP = "auto_stop",
		PREF_DURATION_HOUR = "duration_hour",
		PREF_DURATION_MIN = "duration_min",
		PREF_DURATION_SEC = "duration_sec";

	public final static boolean
		DEF_WARN_BEFORE_CLEARING_TABLE = true,
		DEF_AUTO_STOP = true,
		DEF_AUTO_SWITCH_GRAPH = true;
	public final static int
		DEF_DURATION_HOUR = 0,
		DEF_DURATION_MIN = 0,
		DEF_DURATION_SEC = 10;

	/* duration */
	private final String duration_format;

	public Box0(Context context) {
		super(context);

		Database.prepare(context);

		m_deviceManager = new DeviceManager(context);
		m_deviceManager.setOnStatusChangedListener(this);
		m_prefs = context.getSharedPreferences(PREFS_NAME, 0);

		duration_format = context.getString(R.string.iface_box0_duration_format);

		m_ain = new AinSource(context);
		m_spi = new SpiSource(context);
		m_i2c = new I2cSource(context);
	}

	private final Frontend m_frontend = new Frontend();
	public Frontend getFrontend() {
		return m_frontend;
	}

	private Iface.OnStatusChangedListener m_listener;
	public void setOnStatusChangedListener(Iface.OnStatusChangedListener listener) {
		m_listener = listener;
	}

	private void deviceChanged() {
		Device dev = m_deviceManager.getDevice();

		/* stop if running */
		if (m_running) {
			onStop();
		}

		if (m_ain != null) {
			m_ain.setDevice(dev);
		}

		if (m_spi != null) {
			m_spi.setDevice(dev);
		}

		if (m_i2c != null) {
			m_i2c.setDevice(dev);
		}

		m_frontend.sync();
	}

	private boolean m_running = false;

	public void onStatusChanged(DeviceManager deviceManager, int msg) {
		switch(msg) {
		case DeviceManager.OnStatusChangedListener.DEVICE_CONNECTED:
			/* goto a device */
			Toast.makeText(getContext(),
				R.string.device_connected,
				Toast.LENGTH_SHORT).show();
			deviceChanged();
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_DISCONNECTED:
			/* remove the device */
			/* long length toast because the user only do is look at the toast */
			Toast.makeText(getContext(),
				R.string.device_disconnected,
				Toast.LENGTH_LONG).show();
			deviceChanged();
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_ERROR:
			/* error while processing device */
			showDeviceSearchDialog(R.string.device_error);
		break;
		case DeviceManager.OnStatusChangedListener.PERMISSION_DENIED:
			/* do nothing, permission denied. user dont want to do this */
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_INVALID:
			/* invalid device?? */
			showDeviceSearchDialog(R.string.device_error);
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_NOT_FOUND:
			/* no device found */
			showDeviceSearchDialog(R.string.device_not_found);
		break;
		}
	}

	/* loop in dialog that will prompt user to search for device */
	private void showDeviceSearchDialog(int resIdTitle) {
		Context context = getContext();
		DeviceSearchDialog dialog;
		dialog = new DeviceSearchDialog(m_deviceManager, context, resIdTitle);
		dialog.show();
	}

	private class Frontend extends Iface.Frontend {
		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
						Bundle savedInstanceState) {

			if (Config.DEBUG) Log.d(TAG, "onCreateView");

			View rootView = inflater.inflate(R.layout.iface_box0, container, false);
			m_ain.extractView(rootView);
			m_spi.extractView(rootView);
			m_i2c.extractView(rootView);
			return rootView;
		}

		@Override
		public void onDestroyView() {
			if (Config.DEBUG) Log.d(TAG, "onDestroyView");

			m_ain.extractView(null);
			m_spi.extractView(null);
			m_i2c.extractView(null);

			super.onDestroyView();
		}

		@Override
		public void onResume() {
			super.onResume();

			if (Config.DEBUG) Log.d(TAG, "onResume");

			if (m_deviceManager.getDevice() == null) {
				if (Config.DEBUG)
					Log.d(TAG, "performing a background device search");

				/* search for device in background */
				m_deviceManager.search(true);
			}
		}

		@Override
		public void onDestroyOptionsMenu() {
			m_actionSearchDevice = null;
			m_actionStartStop = null;
			m_actionDuration = null;
			m_actionDeviceInfo = null;
			super.onDestroyOptionsMenu();
		}

		@Override
		public void onPrepareOptionsMenu(Menu menu) {
			super.onPrepareOptionsMenu(menu);
			sync();

			/* clearable */
			boolean warnBeforeClearingTable =
				m_prefs.getBoolean(PREF_WARN_BEFORE_CLEARING_TABLE, true);
			m_actionWarnBeforeClearingTable.setChecked(warnBeforeClearingTable);
		}

		public void sync() {
			if (m_actionSearchDevice == null ||
				m_actionStartStop == null ||
				m_actionDuration == null ||
				m_actionDeviceInfo == null) {
				//TODO: log this
				return;
			}

			Device device = m_deviceManager.getDevice();

			/* device related */
			m_actionSearchDevice.setVisible(device == null);
			m_actionDeviceInfo.setVisible(device != null);

			/* start/stop related */
			m_actionStartStop.setVisible(device != null);
			int icon_res, title_res;
			if (m_running) {
				icon_res = R.drawable.ic_stop_grey600_48dp;
				title_res = R.string.stop;
			} else {
				icon_res = R.drawable.ic_play_grey600_48dp;
				title_res = R.string.start;
			}

			m_actionStartStop.setIcon(icon_res);
			m_actionStartStop.setTitle(title_res);

			/* duration related */
			m_actionDuration.setEnabled(!m_running);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch(item.getItemId()) {
			case R.id.iface_box0_start_stop:
				if (m_running) {
					Box0.this.onStop();
				} else {
					Box0.this.onStart();
				}
			return true;
			case R.id.iface_box0_search_usb_device:
				m_deviceManager.search();
			return true;
			case R.id.iface_box0_duration: {
				DurationPickerDialog dialog =
					new DurationPickerDialog(getActivity());
				dialog.show();
			} return true;
			case R.id.iface_box0_warn_before_clearing_table: {
				boolean newValue = !item.isChecked();
				item.setChecked(newValue);
				SharedPreferences.Editor editor = m_prefs.edit();
				editor.putBoolean(PREF_WARN_BEFORE_CLEARING_TABLE, newValue);
				editor.apply();
			} return true;
			case R.id.iface_box0_auto_switch_graph: {
				boolean newValue = !item.isChecked();
				item.setChecked(newValue);
				SharedPreferences.Editor editor = m_prefs.edit();
				editor.putBoolean(PREF_AUTO_SWITCH_GRAPH, newValue);
				editor.apply();
			} return true;
			case R.id.iface_box0_device_info: {
				Device dev = m_deviceManager.getDevice();
				DeviceInfoDialog dialog = new DeviceInfoDialog(getActivity(), dev);
				dialog.show();
			} return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		private MenuItem
			m_actionSearchDevice,
			m_actionDuration,
			m_actionStartStop,
			m_actionWarnBeforeClearingTable,
			m_actionDeviceInfo;

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.iface_box0_actions, menu);
			m_actionSearchDevice = menu.findItem(R.id.iface_box0_search_usb_device);
			m_actionStartStop = menu.findItem(R.id.iface_box0_start_stop);
			m_actionDuration = menu.findItem(R.id.iface_box0_duration);
			m_actionWarnBeforeClearingTable = menu.findItem(
				R.id.iface_box0_warn_before_clearing_table);
			m_actionDeviceInfo = menu.findItem(R.id.iface_box0_device_info);

			super.onCreateOptionsMenu(menu, inflater);
		}
	}

	private void onStart() {
		boolean warn = m_prefs.getBoolean(
			PREF_WARN_BEFORE_CLEARING_TABLE,
			DEF_WARN_BEFORE_CLEARING_TABLE);
		onStart(warn);
	}

	private final StopOnTimeout stopOnTimeout = new StopOnTimeout();
	private void onStart(boolean warnMe) {
		if (warnMe) {
			/* is their any data to warn user about? */
			if (!Column.hasUsefulData()) {
				/* WARN: if user created some columns without any data,
				 *   this will incorrectly remove them since they contain no data.
				 *   because it based both the conditions. */
				warnMe = false;
			}
		}

		/* warn user that it could loose some data unsaved data. */
		if (warnMe) {
			TableClearConsentDialog dialog =
				new TableClearConsentDialog(getContext());
			dialog.show();
			/* dialog will do onStart(false) [onStart with warnMe=false]
			 *   if user agree to delete data */
			return;
		}

		/* remove all table content */
		Column.removeAllColumn();

		int value = OnStatusChangedListener.INFINITE;
		boolean autoStop = m_prefs.getBoolean(PREF_AUTO_STOP, DEF_AUTO_STOP);
		if (autoStop) {
			int hour = m_prefs.getInt(PREF_DURATION_HOUR, DEF_DURATION_HOUR);
			int min = m_prefs.getInt(PREF_DURATION_MIN, DEF_DURATION_MIN);
			int sec = m_prefs.getInt(PREF_DURATION_SEC, DEF_DURATION_SEC);
			value = (hour * 60 * 60) + (min * 60) + sec;
			if (Config.DEBUG) Log.d(TAG, "perform logging for " + value + " seconds");
		}

		/* Why not register these modules as OnStatusChangedListener? */
		if (!startModules(value)) {
			/* force stop all */
			m_ain.setTime(OnStatusChangedListener.STOP);
			m_spi.setTime(OnStatusChangedListener.STOP);
			m_i2c.setTime(OnStatusChangedListener.STOP);
			return;
		}

		if (m_listener != null) {
			m_listener.onStatusChanged(this, value);
		}

		stopOnTimeout.start(value);

		boolean autoSwitchGraph = m_prefs.getBoolean(
			PREF_AUTO_SWITCH_GRAPH, DEF_AUTO_SWITCH_GRAPH);
		if (autoSwitchGraph) {
			/* TODO: show graph */
		}

		m_running = true;
		m_frontend.sync();
	}

	private boolean startModules(int value) {
		int run = 0, status;

		status = m_ain.setTime(value);
		if (status < 0) {
			if (Config.ERROR) Log.e(TAG, "AIN start failed");
			onStartError(R.string.ain_start_failed);
			return false;
		} else if (status > 0) {
			run++;
		}

		status = m_spi.setTime(value);
		if (status < 0) {
			if (Config.ERROR) Log.e(TAG, "SPI start failed");
			onStartError(R.string.spi_start_failed);
			return false;
		} else if (status > 0) {
			run++;
		}

		status = m_i2c.setTime(value);
		if (status < 0) {
			if (Config.ERROR) Log.e(TAG, "I2C start failed");
			onStartError(R.string.i2c_start_failed);
			return false;
		} else if (status > 0) {
			run++;
		}

		if (run == 0) {
			NothingToStartDialog dialog = new NothingToStartDialog(getContext());
			dialog.show();
			return false;
		}

		return true;
	}

	private void onStartError(int err_res) {
		StartErrorDialog dialog = new StartErrorDialog(getContext(), err_res);
		dialog.show();
	}

	private void onStop() {
		m_ain.setTime(OnStatusChangedListener.STOP);
		m_spi.setTime(OnStatusChangedListener.STOP);
		m_i2c.setTime(OnStatusChangedListener.STOP);

		if (m_listener != null) {
			m_listener.onStatusChanged(this, OnStatusChangedListener.STOP);
		}

		m_running = false;
		m_frontend.sync();
	}

	public class TableClearConsentDialog extends RemoveAllDialog {
		public TableClearConsentDialog(Context context) {
			super(context);
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				Box0.this.onStart(false);
			}
		}
	}

	public class StartErrorDialog extends AlertDialog
						implements DialogInterface.OnClickListener {
		public StartErrorDialog(Context context, int msg_res) {
			super(context);

			setIcon(R.drawable.ic_alert_circle_grey600_48dp);
			setTitle(context.getString(R.string.start_failed));
			setMessage(context.getString(msg_res));

			setButton(DialogInterface.BUTTON_POSITIVE,
				context.getString(R.string.ok), this);
		}

		public void onClick(DialogInterface dialog, int which) { }
	}



	private class StopOnTimeout extends Handler {
		Runnable task = new Runnable () {
			public void run() {
				m_running = false;
				m_frontend.sync();
			}
		};

		public StopOnTimeout() {
			super();
		}

		public void start(int time) {
			if (time > 0) {
				postDelayed(task, time * 1000);
			}
		}

		public void stop() {
			removeCallbacks(task);
		}
	}

	/**
	 * Route the indent to device manager.
	 * @return true if a new device is attached.
	 */
	@Override
	public boolean consumeIndent(Intent intent) {
		super.consumeIndent(intent);

		boolean before = m_deviceManager.getDevice() != null;
		m_deviceManager.consumeIndent(intent);
		boolean after = m_deviceManager.getDevice() != null;

		/* user will be most likely interested
		 *  because a device has been attached.
		 */
		return (!before && after);
	}

	static {
		Iface.register(Box0.class);
	}
}
