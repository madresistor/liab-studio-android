/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.i2c;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.I2c;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Source;
import com.madresistor.liab_studio.iface.box0.source.common.SensorTable;
import com.madresistor.liab_studio.iface.box0.source.common.SlowSamplingMode;
import com.madresistor.liab_studio.iface.box0.source.i2c.Logger;
import com.madresistor.liab_studio.iface.box0.source.i2c.SensorAdapter;

public class I2cSource extends Source implements View.OnClickListener {
	private final static String TAG = I2cSource.class.getName();
	private SensorAdapter m_sensorAdapter;
	private SlowSamplingMode m_masterMode;
	private I2c m_i2c;
	private final Logger m_logger;

	public I2cSource(Context context) {
		super(context);
		m_sensorAdapter = new SensorAdapter(context);
		m_logger = new Logger(context);
	}

	public void extractView(View rootView) {
		if (rootView == null) {
			m_masterMode = null;
			return;
		}

		m_masterMode = (SlowSamplingMode) rootView.findViewById(
			R.id.iface_box0_i2c_master_mode);

		SensorTable sensorTable = (SensorTable) rootView.findViewById(
			R.id.iface_box0_i2c_sensor_table);
		m_sensorAdapter.attachTo(sensorTable);

		rootView.findViewById(R.id.iface_box0_i2c_sensor_add)
			.setOnClickListener(this);
	}

	public int setTime(int time) {
		if (time < 0) {
			return m_logger.stop();
		}

		if (m_i2c == null) {
			if (Config.WARN) Log.w(TAG, "m_i2c is null");
			return -1;
		}

		int status = m_logger.prepare(m_sensorAdapter, m_i2c);
		if (status <= 0) {
			return status;
		}

		int sample_rate = m_masterMode.getValue();
		return m_logger.start(time, sample_rate);
	}

	public void setDevice(Device dev) {
		close();
		if (dev != null) {
			open(dev);
		}
	}

	private void close() {
		/* try to stop logger if running */
		m_logger.stop();

		if (m_i2c != null) {
			try {
				m_i2c.close();
			} catch (ResultException e) {

			} finally {
				m_i2c = null;
			}
		}
	}

	private void open(Device dev) {
		try {
			m_i2c = dev.i2c(0);
		} catch(ResultException e) {
			m_i2c = null;
			if (Config.WARN) Log.w(TAG, "Unable to open module");
		}
	}

	/* add button clicked */
	public void onClick(View view) {
		AddSensorDialog dialog = new AddSensorDialog(
			getContext(), m_sensorAdapter);
		dialog.show();
	}
}
