/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.ain;

import android.content.Context;

import com.madresistor.box0.module.Ain;
import com.madresistor.liab_studio.Extra;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.iface.box0.source.ain.SensorAdapter;
import com.madresistor.liab_studio.tunnel.Column;
import com.madresistor.liab_studio.tunnel.Curve;
import com.madresistor.liab_studio.JniLoader;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Logger {
	private final static String TAG = Logger.class.getName();

	/* only applicable when sensor used on Earth */
	private final static double
		ACCELERATION_DUE_TO_GRAVITY = 9.8067;

	public static class Entry {
		public int channel;
		public int averaging;
		public String transferFunction;
		public int col_time, col_value;
		public int type;
	};

	private final Context m_context;
	private final String m_prefix_time,
		m_prefix_value, m_unknown_name_format, m_curve_name_format;
	public Logger(Context context) {
		m_context = context;
		m_prefix_time = context.getString(R.string.iface_box0_column_suffix_time);
		m_prefix_value = context.getString(R.string.iface_box0_column_suffix_value);
		m_unknown_name_format = context.getString(R.string.iface_box0_ain_unknown_log_name_format);
		m_curve_name_format = m_context.getString(R.string.widget_curve_name_format);
	}

	private Entry prepareEntry(int chan, SensorAdapter.Sensor sensor) {
		Entry entry = new Entry();
		entry.channel = chan;
		entry.averaging = sensor.averaging;

		String sensorName = buildSensorName(sensor);

		if (sensor.transferFunction != null && !sensor.transferFunction.isEmpty()) {
			entry.transferFunction = sensor.transferFunction;
		}

		/* time */
		entry.col_time = Column.appendColumn();
		Column.setName(entry.col_time, sensorName + m_prefix_time);
		Column.setColor(entry.col_time, Extra.newColorForUser());

		/* value */
		entry.col_value = Column.appendColumn();
		Column.setName(entry.col_value, sensorName + m_prefix_value);
		int color = Extra.newColorForUser();
		Column.setColor(entry.col_value, color);

		/* curve */
		int crv = Curve.appendCurve(entry.col_time, entry.col_value);
		String name = String.format(m_curve_name_format,
			Column.getName(entry.col_value),
			Column.getName(entry.col_time));
		Curve.setName(crv, name);
		Curve.setColor(crv, color);
		Curve.setLineWidth(crv, Column.getLineWidth(entry.col_value));

		/* sensor information */
		entry.type = Database.ain.get(sensor.database_index).type;

		return entry;
	}

	private String buildSensorName(SensorAdapter.Sensor sensor) {
		if (sensor.column != null && !sensor.column.isEmpty()) {
			return sensor.column;
		}

		String family = m_context.getString(R.string.ain);
		return String.format(m_unknown_name_format, family, sensor.channel);
	}

	public int start(SensorAdapter adapter, Ain ain, int time, int bitsize,
				long speed) {
		List<Entry> entries = new ArrayList<Entry>();
		for (int i = 0; i < adapter.getCount(); i++) {
			SensorAdapter.Sensor sensor = adapter.getSensor(i);
			if (sensor.selected) {
				Entry entry = prepareEntry(i, sensor);
				if (entry == null) {
					return -1;
				}

				entries.add(entry);
			}
		}

		int count = entries.size();
		if (count == 0) {
			return 0;
		}

		Entry[] entriesArray = entries.toArray(new Entry[count]);
		return start(ain.getPointer(), entriesArray, (int)time, bitsize, speed);
	}

	private static native int start(
		ByteBuffer /* (b0_ain*) */ ain,
		Entry[] entries, int time,
		int bitsize, long speed);
	public static native int stop();

	static {
		JniLoader.glue();
	}
}
