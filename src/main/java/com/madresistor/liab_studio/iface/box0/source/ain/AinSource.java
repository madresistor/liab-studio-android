/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.ain;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Ain;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Source;
import com.madresistor.liab_studio.iface.box0.source.ain.Logger;
import com.madresistor.liab_studio.iface.box0.source.ain.SensorAdapter;
import com.madresistor.liab_studio.iface.box0.source.ain.StreamMode;
import com.madresistor.liab_studio.iface.box0.source.common.SensorTable;

public class AinSource extends Source  {
	private final static String TAG = AinSource.class.getName();
	private StreamMode m_streamMode;
	private final SensorAdapter m_sensorAdapter;
	private Ain m_ain;
	private final Logger m_logger;

	public AinSource(Context context) {
		super(context);
		m_sensorAdapter = new SensorAdapter(context);
		m_logger = new Logger(context);
	}

	public int setTime(int time) {
		if (time < 0) {
			return m_logger.stop();
		}

		if (m_ain == null) {
			if (Config.WARN) Log.w(TAG, "m_ain is null");
			return -1;
		}

		int bitsize = m_streamMode.getBitsize();
		long speed = m_streamMode.getSpeed();

		if (bitsize <= 0) {
			if (Config.WARN) Log.w(TAG, "Invalid bitsize");
			return -1;
		}

		if (speed <= 0) {
			if (Config.WARN) Log.w(TAG, "Invalid speed");
			return -1;
		}

		return m_logger.start(m_sensorAdapter, m_ain, time, bitsize, speed);
	}

	public void setDevice(Device dev) {
		close();
		if (dev != null) {
			open(dev);
		}

		update();
	}

	private void close() {
		/* try to stop logger if running */
		m_logger.stop();

		if (m_ain != null) {
			try {
				m_ain.close();
			} catch (ResultException e) {

			} finally {
				m_ain = null;
			}
		}
	}

	private void open(Device dev) {
		try {
			m_ain = dev.ain(0);
		} catch(ResultException e) {
			m_ain = null;
			if (Config.WARN) Log.w(TAG, "Unable to open module");
		}
	}

	public void extractView(View rootView) {
		if (rootView == null) {
			m_streamMode = null;
			return;
		}

		m_streamMode = (StreamMode) rootView.findViewById(
			R.id.iface_box0_ain_stream_mode);

		SensorTable sensorTable = (SensorTable) rootView.findViewById(
			R.id.iface_box0_ain_sensor_table);
		m_sensorAdapter.attachTo(sensorTable);

		update();
	}

	private void update() {
		if (m_streamMode != null) {
			m_streamMode.setModule(m_ain);
		}

		m_sensorAdapter.setModule(m_ain);
	}
}
