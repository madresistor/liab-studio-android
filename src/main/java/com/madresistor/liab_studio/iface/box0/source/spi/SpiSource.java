/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.spi;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.linearlistview.LinearListView;
import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Spi;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Source;
import com.madresistor.liab_studio.iface.box0.source.common.SlowSamplingMode;
import com.madresistor.liab_studio.iface.box0.source.spi.Logger;

public class SpiSource extends Source {
	private final static String TAG = SpiSource.class.getName();
	private SensorAdapter m_sensorAdapter;
	private SlowSamplingMode m_masterMode;
	private Spi m_spi;
	private final Logger m_logger;

	public SpiSource(Context context) {
		super(context);
		m_sensorAdapter = new SensorAdapter(context);
		m_logger = new Logger(context);
	}

	public int setTime(int time) {
		if (time < 0) {
			return m_logger.stop();
		}

		if (m_spi == null) {
			if (Config.WARN) Log.w(TAG, "m_spi is null");
			return -1;
		}

		int status = m_logger.prepare(m_sensorAdapter, m_spi);
		if (status <= 0) {
			return status;
		}

		int sample_rate = m_masterMode.getValue();
		return m_logger.start(time, sample_rate);
	}

	public void setDevice(Device dev) {
		close();
		if (dev != null) {
			open(dev);
		}

		update();
	}

	private void close() {
		if (m_spi != null) {
			try {
				m_spi.close();
			} catch (ResultException e) {

			} finally {
				m_spi = null;
			}
		}
	}

	private void open(Device dev) {
		try {
			m_spi = dev.spi(0);
		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "Unable to open module");
			m_spi = null;
		}
	}

	public void extractView(View rootView) {
		if (rootView == null) {
			m_masterMode = null;
			return;
		}

		LinearListView sensorTable = (LinearListView) rootView.findViewById(
			R.id.iface_box0_spi_sensor_table);
		m_sensorAdapter.attachTo(sensorTable);

		m_masterMode = (SlowSamplingMode) rootView.findViewById(
			R.id.iface_box0_spi_master_mode);

		update();
	}

	private void update() {
		m_sensorAdapter.setModule(m_spi);
	}
}
