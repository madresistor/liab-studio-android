/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.ain;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Button;
import android.util.Log;

import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.iface.box0.source.ain.SensorAdapter;
import com.madresistor.liab_studio.iface.box0.source.ain.SensorType;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.Config;

public class EditSensorDialog extends AlertDialog implements
				DialogInterface.OnClickListener,
				AdapterView.OnItemSelectedListener,
				View.OnClickListener {
	private static final String TAG = EditSensorDialog.class.getName();
	private SensorAdapter m_adapter;
	private SensorAdapter.Sensor m_sensor;

	private final SensorType m_type;
	private final EditText m_averaging;
	private final EditText m_transferFunction;
	private final EditText m_column;

	public EditSensorDialog(Context context, SensorAdapter adapter, SensorAdapter.Sensor sensor) {
		super(context);
		m_sensor = sensor;
		m_adapter = adapter;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.iface_box0_ain_sensor, null);
		setView(rootView);

		/* type */
		m_type = (SensorType) rootView.findViewById(
			R.id.iface_box0_ain_sensor_type);

		m_type.setSelection(sensor.database_index);
		m_type.setOnItemSelectedListener(this);

		/* averaging  */
		m_averaging = (EditText) rootView.findViewById(
			R.id.iface_box0_ain_sensor_averaging);
		if (sensor.averaging < 1) {
			sensor.averaging = 1;
		}
		m_averaging.setText(Integer.toString(sensor.averaging));

		/* transfer function */
		m_transferFunction = (EditText) rootView.findViewById(
			R.id.iface_box0_ain_sensor_transfer_function);
		m_transferFunction.setText(sensor.transferFunction);

		/* column */
		m_column = (EditText) rootView.findViewById(
			R.id.iface_box0_ain_sensor_column);
		m_column.setText(sensor.column);

		int res_id;

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.done), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);

		/* title */
		setTitle(sensor.channel);

		/* title */
		setIcon(R.drawable.ic_pencil_grey600_48dp);
	}

	/* POSITIVE_BUTTON custom click handling so that when user click Ok,
	 *  testing of transfer function is done.
	 * if test failed, show error and do not close the alert dialog */
	public void onClick(View v) {
		String tfn = m_transferFunction.getText().toString();
		String result = null;

		if (tfn.isEmpty()) {
			tfn = null;
		}

		if (tfn != null) {
			result = Database.transferFuncTest(tfn);
		}

		if (result == null) {
			onClick(this, DialogInterface.BUTTON_POSITIVE);
			dismiss();
		} else {
			/* we have problem */
			Context context = getContext();
			new AlertDialog.Builder(context)
			.setTitle(context.getString(R.string.transfer_function_problem))
			.setMessage(result)
			.setNeutralButton(R.string.back, null)
			.show();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		Button btn = getButton(DialogInterface.BUTTON_POSITIVE);
		btn.setOnClickListener(this);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			m_sensor.database_index = m_type.getSelectedItemPosition();
			String avgText = m_averaging.getText().toString();
			int avgVal = 1;
			if (avgText != null && !avgText.isEmpty()) {
				try {
					avgVal = Integer.parseInt(avgText);
				} catch(Exception e) {
					if (Config.WTF) Log.wtf(TAG, "Integer.parseInt(\""+ avgText +"\") throwed exception", e);
				}
			}

			if (avgVal < 1) {
				avgVal = 1;
			}
			m_sensor.averaging = avgVal;

			m_sensor.transferFunction = m_transferFunction.getText().toString();
			m_sensor.column = m_column.getText().toString();
			m_adapter.notifyDataSetChanged();
		break;
		case DialogInterface.BUTTON_NEUTRAL:
			dismiss();
		break;
		}
	}

	/*
	 * ignore the first item selection because it is triggered by setSelection
	 */
	private boolean m_ignoreItemSelection = true;

	/* when user select another Type, update TransferFunction accordingly */
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (m_ignoreItemSelection) {
			m_ignoreItemSelection = false;
			return;
		}

		String v = Database.ain.get(position).func;
		m_transferFunction.setText(v.toString());
	}

	public void onNothingSelected(AdapterView<?> parent) {
		//bla
	}
}
