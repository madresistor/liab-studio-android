/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.linearlistview.LinearListView;
import com.madresistor.liab_studio.R;

/* a generic interface to sensors using ListView */
public abstract class SensorBaseAdapter extends BaseAdapter
			implements LinearListView.OnItemLongClickListener,
						LinearListView.OnItemClickListener {
	private final LayoutInflater m_inflater;
	private final Context m_context;

	public SensorBaseAdapter(Context context) {
		m_context = context;
		m_inflater = LayoutInflater.from(context);
	}

	public Context getContext() {
		return m_context;
	}

	/* this will perform all necessary work */
	public void attachTo(LinearListView linearListView) {
		linearListView.setOnItemLongClickListener(this);
		linearListView.setOnItemClickListener(this);
		linearListView.setAdapter(this);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = m_inflater.inflate(
				R.layout.iface_box0_sensor , parent, false);
		}

		CheckedTextView selected;
		TextView type, column;

		selected = (CheckedTextView) convertView.findViewById(
			R.id.iface_box0_sensor_selected);

		type = (TextView) convertView.findViewById(
			R.id.iface_box0_sensor_type);

		column = (TextView) convertView.findViewById(
			R.id.iface_box0_sensor_column);

		selected.setChecked(isSelected(position));
		selected.setText(getTextTop(position));
		type.setText(getTextLeft(position));
		column.setText(getTextRight(position));

		return convertView;
	}

	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	/* item is selected/unselected by clicking it */
	public void onItemClick (LinearListView parent, View view,
			int position, long id) {
		itemClicked(view, position);
	}

	private void itemClicked(View view, int position) {
		Checkable enable = (Checkable) view.findViewById(
			R.id.iface_box0_sensor_selected);
		enable.toggle();
		setSelected(position, enable.isChecked());
	}

	/* long press it to edit */
	public void onItemLongClick(LinearListView parent, View view,
			int position, long id) {
		editSensor(position);
	}

	//public abstract int getCount();
	public abstract void setSelected(int index, boolean selected);
	public abstract boolean isSelected(int index);
	public abstract String getTextTop(int index);
	public abstract String getTextRight(int index);
	public abstract String getTextLeft(int index);

	/* this is called when user want to edit sensor */
	public abstract void editSensor(int index);
}
