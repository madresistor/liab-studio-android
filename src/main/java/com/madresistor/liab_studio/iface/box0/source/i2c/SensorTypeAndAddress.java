/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.i2c;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.iface.box0.source.common.TypeBaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class SensorTypeAndAddress extends LinearLayout {
	public SensorTypeAndAddress(Context context) {
		super(context);
		init(context);
	}

	public SensorTypeAndAddress(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public void setUsedAddresses(int[] addresses, int prev_addr) {
		List<Database.I2c> list = new ArrayList<Database.I2c>();
		List<Database.I2c.Address> alist = new ArrayList<Database.I2c.Address>();

		for (Database.I2c i2c: Database.i2c) {
			alist.clear();
			for (Database.I2c.Address addr: i2c.addresses) {
				boolean include = true;
				if (addr.value != prev_addr) {
					for (int i = 0; i < addresses.length; i++) {
						if (addresses[i] == addr.value) {
							include = false;
							break;
						}
					}
				}

				/* if the element was not found in used address list, then add this to list */
				if (include) {
					alist.add(addr);
				}
			}

			/* add the i2c to list if their is some address */
			if (alist.size() > 0) {
				Database.I2c.Address[] i2c_addr = new Database.I2c.Address[alist.size()];
				i2c_addr = alist.toArray(i2c_addr);
				list.add(new Database.I2c(i2c.type, i2c.name, i2c_addr));
			}
		}

		setFiltered(list);
	}

	private List<Database.I2c> m_filteredList;
	private void setFiltered(List<Database.I2c> list) {
		/* FIX: empty list if their no more selectable (free addresses) */
		m_filteredList = list;
		m_typeAdapter.setFiltered(list);
		m_addressAdapter.setFiltered(list);
	}

	private void init(Context context) {
		m_typeAdapter = new TypeAdapter(context);
		m_addressAdapter = new AddressAdapter(context);
		setFiltered(Database.i2c); // default i2c list
	}

	private TypeAdapter m_typeAdapter;
	private AddressAdapter m_addressAdapter;
	private Spinner m_typeIndex, m_address;

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		m_typeIndex = (Spinner) findViewById(R.id.iface_box0_i2c_sensor_type);
		m_address = (Spinner) findViewById(R.id.iface_box0_i2c_sensor_address);

		m_typeIndex.setAdapter(m_typeAdapter);
		m_address.setAdapter(m_addressAdapter);

		m_typeIndex.setOnItemSelectedListener(m_addressAdapter);
	}

	public int getType() {
		int pos = m_typeIndex.getSelectedItemPosition();
		return m_filteredList.get(pos).type;
	}

	public int getAddress() {
		int position = m_typeIndex.getSelectedItemPosition();
		int addr_index = m_address.getSelectedItemPosition();
		return m_filteredList.get(position).addresses[addr_index].value;
	}

	public void setType(int type) {
		for (int i = 0; i < m_filteredList.size(); i++) {
			if (m_filteredList.get(i).type == type) {
				m_typeIndex.setSelection(i);
				break;
			}
		}
	}

	public void setAddress(int address) {
		int index = m_typeIndex.getSelectedItemPosition();
		Database.I2c.Address[] addresses = m_filteredList.get(index).addresses;
		for (int i = 0; i < addresses.length; i++) {
			if (address == addresses[i].value) {
				m_address.setSelection(i);
				break;
			}
		}

		/* TODO: log warn? */
	}
}

abstract class CommonAdapter extends TypeBaseAdapter {
	public CommonAdapter(Context context) {
		super(context);
	}

	protected List<Database.I2c> m_filteredList = null;
	public void setFiltered(List<Database.I2c> i2c) {
		m_filteredList = i2c;
		notifyDataSetChanged();
	}
}

class TypeAdapter extends CommonAdapter {
	public TypeAdapter(Context context) {
		super(context);
	}

	@Override
	public Object getItem(int position) {
		return m_filteredList.get(position).name;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getCount() {
		if (m_filteredList == null) {
			return 0;
		}

		return m_filteredList.size();
	}
}

class AddressAdapter extends CommonAdapter implements AdapterView.OnItemSelectedListener {
	private final String m_format;

	public AddressAdapter(Context context) {
		super(context);
		m_format = context.getString(R.string.iface_box0_i2c_address_name_value_format);
	}

	@Override
	public Object getItem(int position) {
		Database.I2c.Address addr = m_filteredList.get(m_typeIndex).addresses[position];
		return String.format(m_format, addr.name, addr.value);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getCount() {
		if (m_typeIndex < 0 || m_filteredList == null) {
			return 0;
		}

		return m_filteredList.get(m_typeIndex).addresses.length;
	}

	/* from Type */
	private int m_typeIndex = -1;

	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		/* filteredList is same for Address and Type spinner */
		m_typeIndex = position;
		notifyDataSetChanged();
	}

	public void onNothingSelected(AdapterView<?> parent) {
		//bla
	}
}
