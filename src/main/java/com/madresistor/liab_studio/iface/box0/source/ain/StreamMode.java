/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.ain;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Ain;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.R;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class StreamMode extends LinearLayout {
	private Spinner m_bitsize, m_speed;
	private BitsizeAdapter m_bitsizeAdapter;
	private SpeedAdapter m_speedAdapter;
	private Ain m_ain;
	private List<Integer> m_bitsize_values = new ArrayList<Integer>();
	private Map<Integer, List<Long> > m_speed_values = new Hashtable<Integer, List<Long> >();
	private final static String TAG = StreamMode.class.getName();

	public StreamMode(Context context) {
		super(context);
		init(context);
	}

	public StreamMode(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		m_speedAdapter = new SpeedAdapter(context);
		m_bitsizeAdapter = new BitsizeAdapter(context);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		Context context = getContext();
		m_bitsize = (Spinner) findViewById(R.id.iface_box0_ain_stream_mode_bitsize);
		m_speed = (Spinner) findViewById(R.id.iface_box0_ain_stream_mode_speed);

		m_speed.setAdapter(m_speedAdapter);
		m_bitsize.setAdapter(m_bitsizeAdapter);
		m_bitsize.setOnItemSelectedListener(m_speedAdapter);

		m_bitsize.setSelection(0);
	}

	public int getBitsize() {
		if (m_bitsize == null) {
			return -1;
		}

		/* get bitsize */
		int bs_index = m_bitsize.getSelectedItemPosition();
		if (!(bs_index >= 0 && bs_index < m_bitsize_values.size())) {
			return -1;
		}

		return m_bitsize_values.get(bs_index);
	}

	public long getSpeed() {
		int bs = getBitsize();

		if (bs <= 0) {
			return -1;
		}

		if (m_speed == null) {
			return -1;
		}

		/* get speed */
		List<Long> speeds = m_speed_values.get(bs);
		if (speeds == null) {
			return -1;
		}

		int speed_index = m_speed.getSelectedItemPosition();
		if (!(speed_index >= 0 && speed_index < speeds.size())) {
			return -1;
		}

		return speeds.get(speed_index);
	}

	public void setModule(Ain ain) {
		m_ain = ain;
		if (ain == null) {
			return;
		}

		m_bitsize_values.clear();
		m_speed_values.clear();

		/* store the values in an array */

		for (Ain.BitsizeSpeeds bss: ain.stream) {
			m_bitsize_values.add(bss.bitsize);

			List<Long> speeds = new ArrayList<Long>();
			m_speed_values.put(bss.bitsize, speeds);

			for (long speed: bss.speed) {
				speeds.add(speed);
			}
		}

		m_bitsizeAdapter.notifyDataSetChanged();
		m_speedAdapter.notifyDataSetChanged();
	}

	private class SpeedAdapter extends CommonAdapter
			implements AdapterView.OnItemSelectedListener {

		private final String m_format_sps, m_format_ksps, m_format_msps;
		private int m_bitsize = -1;

		public SpeedAdapter(Context context) {
			super(context);
			m_format_sps = context.getString(R.string.speed_format_sps);
			m_format_ksps = context.getString(R.string.speed_format_ksps);
			m_format_msps = context.getString(R.string.speed_format_msps);
		}

		public Object getItem(int position) {
			List<Long> speeds = m_speed_values.get(m_bitsize);
			if (speeds == null) {
				if (Config.WTF) Log.wtf(TAG, "bitsize " + m_bitsize + " dont exists in array");
				return ""; //??
			}

			if (!(position < speeds.size())) {
				if (Config.WTF) Log.wtf(TAG, "position " + position + " is out of range");
				return ""; //??
			}

			/* TODO: this method will incorrectly remove all fractional part
			 *    and that could cause problem to user */
			long speed = speeds.get(position);
			if (speed < 1000) {
				return String.format(m_format_sps, speed);
			} else if (speed < 1000000) {
				return String.format(m_format_ksps, speed / 1000);
			} else {
				return String.format(m_format_msps, speed / 1000000);
			}
		}

		public int getCount() {
			List<Long> speeds = m_speed_values.get(m_bitsize);
			return (speeds != null) ? speeds.size() : 0;
		}

		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			/* extract bitsize */
			if (Config.DEBUG) Log.d(TAG, "item selected: position " + position);

			if (position < m_bitsize_values.size()) {
				m_bitsize = m_bitsize_values.get(position);
			} else {
				if (Config.WTF) Log.wtf(TAG, "position " + position + " is out of range");
				m_bitsize = -1; //??
			}

			notifyDataSetChanged();
		}

		public void onNothingSelected(AdapterView<?> parent) {
			/* not interested */
		}
	}

	private class BitsizeAdapter extends CommonAdapter {
		private final String m_format;

		public BitsizeAdapter(Context context) {
			super(context);
			m_format = context.getString(R.string.bitsize_format);
		}

		public Object getItem(int position) {
			return String.format(m_format, m_bitsize_values.get(position));
		}

		public int getCount() {
			return m_bitsize_values.size();
		}
	}

	private abstract class CommonAdapter extends BaseAdapter {
		private final LayoutInflater m_inflater;

		public CommonAdapter(Context context) {
			m_inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getView(android.R.layout.simple_spinner_item,
				position, convertView, parent);
		}

		@Override
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			return getView(android.R.layout.simple_list_item_single_choice,
				position, convertView, parent);
		}

		private View getView(int layout, int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(layout, parent, false);
			}

			String name = getItem(position).toString();
			TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
			textView.setText(name);

			return convertView;
		}

		public long getItemId(int position) {
			return position;
		}
	}
}
