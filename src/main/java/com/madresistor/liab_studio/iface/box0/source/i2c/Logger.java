/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.i2c;

import android.content.Context;
import android.util.Log;

import com.madresistor.box0.ResultException;
import com.madresistor.box0.driver.Adxl345;
import com.madresistor.box0.driver.Bmp180;
import com.madresistor.box0.driver.Driver;
import com.madresistor.box0.driver.L3gd20;
import com.madresistor.box0.driver.Lm75;
import com.madresistor.box0.driver.Si114x;
import com.madresistor.box0.driver.Tmp006;
import com.madresistor.box0.driver.Tsl2591;
import com.madresistor.box0.module.I2c;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.Extra;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.iface.box0.source.i2c.SensorAdapter;
import com.madresistor.liab_studio.tunnel.Column;
import com.madresistor.liab_studio.tunnel.Curve;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Logger implements Runnable {
	private final static String TAG = Logger.class.getName();

	/* only applicable when sensor used on Earth */
	private final static double
		ACCELERATION_DUE_TO_GRAVITY = 9.8067;

	public static class Entry {
		public int type;
		public int col_time;
		public int[] cols_value;
		public Driver driver;

		public int sample_total, sample_got;
	};

	private final Context m_context;
	private final String m_prefix_time,
		m_prefix_x, m_prefix_y, m_prefix_z, m_prefix_value,
		m_unknown_name_format, m_curve_name_format;
	public Logger(Context context) {
		m_context = context;
		m_prefix_time = context.getString(R.string.iface_box0_column_suffix_time);
		m_prefix_value = context.getString(R.string.iface_box0_column_suffix_value);
		m_prefix_x = context.getString(R.string.iface_box0_column_suffix_x);
		m_prefix_y = context.getString(R.string.iface_box0_column_suffix_y);
		m_prefix_z = context.getString(R.string.iface_box0_column_suffix_z);
		m_unknown_name_format = context.getString(R.string.iface_box0_i2c_unknown_log_name_format);
		m_curve_name_format = m_context.getString(R.string.widget_curve_name_format);
	}

	private List<Entry> m_entries = new ArrayList<Entry>();

	public int prepare(SensorAdapter adapter, I2c i2c) {
		/* Try to prepare module for master mode */
		try {
			i2c.masterPrepare();
		} catch (ResultException e) {
			if (Config.WARN) Log.w(TAG, "Unable to prepare I2C for master mode");
			return -1;
		}

		m_entries.clear(); //make sure that m_entries is empty

		for (int i = 0; i < adapter.getCount(); i++) {
			SensorAdapter.Sensor sensor = adapter.getSensor(i);
			if (sensor.selected) {
				Entry entry = prepareEntry(sensor, i2c);
				if (entry == null) {
					return -1;
				}

				m_entries.add(entry);
			}
		}

		return m_entries.isEmpty() ? 0 : 1;
	}

	private Entry prepareEntry(SensorAdapter.Sensor sensor, I2c i2c) {
		Entry entry = new Entry();
		entry.type = sensor.type;

		String sensorName = buildSensorName(sensor);

		entry.col_time = Column.appendColumn();
		Column.setName(entry.col_time, sensorName + m_prefix_time);

		try {
			switch(entry.type) {
			case Database.I2c.ADXL345: {
				boolean ALT_ADDRESS = sensor.address == 0x1D;
				Adxl345 driver = new Adxl345(i2c, ALT_ADDRESS);
				driver.powerUp();
				entry.driver = driver;
				int x = Column.appendColumn();
				int y = Column.appendColumn();
				int z = Column.appendColumn();
				Column.setName(x, sensorName + m_prefix_x);
				Column.setName(y, sensorName + m_prefix_y);
				Column.setName(z, sensorName + m_prefix_z);
				entry.cols_value = new int[] { x, y, z };
			} break;
			case Database.I2c.BMP180: {
				entry.driver = new Bmp180(i2c);
				int c = Column.appendColumn();
				Column.setName(c, sensorName + m_prefix_value);
				entry.cols_value = new int[] { c };
			} break;
			case Database.I2c.TMP006: {
				boolean ADR1 = (sensor.address & 0x04) != 0;
				Tmp006.Adr0 ADR0 = Tmp006.Adr0.reverseLookup(sensor.address);
				entry.driver = new Tmp006(i2c, Tmp006.DUMMY_CALIB_FACTOR, ADR1, ADR0);
				int c = Column.appendColumn();
				Column.setName(c, sensorName + m_prefix_value);
				entry.cols_value = new int[] { c };
			} break;
			case Database.I2c.TSL2591: {
				entry.driver = new Tsl2591(i2c);
				int c = Column.appendColumn();
				Column.setName(c, sensorName + m_prefix_value);
				entry.cols_value = new int[] { c };
			} break;
			case Database.I2c.LM75: {
				boolean A2 = (sensor.address & 0x04) != 0;
				boolean A1 = (sensor.address & 0x02) != 0;
				boolean A0 = (sensor.address & 0x01) != 0;
				entry.driver = new Lm75(i2c, A2, A1, A0);
				int c = Column.appendColumn();
				Column.setName(c, sensorName + m_prefix_value);
				entry.cols_value = new int[] { c };
			} break;
			case Database.I2c.L3GD20: {
				boolean SDA0 = sensor.address == 0x6B;
				L3gd20 driver = new L3gd20(i2c, SDA0);
				driver.powerUp();
				entry.driver = driver;
				int x = Column.appendColumn();
				int y = Column.appendColumn();
				int z = Column.appendColumn();
				Column.setName(x, sensorName + m_prefix_x);
				Column.setName(y, sensorName + m_prefix_y);
				Column.setName(z, sensorName + m_prefix_z);
				entry.cols_value = new int[] { x, y, z };
			} break;
			case Database.I2c.SI114X: {
				entry.driver = new Si114x(i2c);
				int c = Column.appendColumn();
				Column.setName(c, sensorName + m_prefix_value);
				entry.cols_value = new int[] { c };
			} break;
			default: {
				if (Config.WTF) Log.wtf(TAG, "entry.type (" + entry.type + ") is unknown (smells like driver missing)");
			} return null;
			}
		} catch(ResultException e) {
			if (Config.ERROR) Log.e(TAG, "ResultException: " + e.toString());
			/* TODO: show error dialog */
			return null;
		}

		/* assign colors and then */
		Column.setColor(entry.col_time, Extra.newColorForUser());
		for (int c: entry.cols_value) {
			int color = Extra.newColorForUser();
			Column.setColor(c, color);

			int crv = Curve.appendCurve(entry.col_time, c);
			String name = String.format(m_curve_name_format,
				Column.getName(c),
				Column.getName(entry.col_time));
			Curve.setName(crv, name);
			Curve.setColor(crv, color);
			Curve.setLineWidth(crv, Column.getLineWidth(c));
		}

		return entry;
	}

	private String buildSensorName(SensorAdapter.Sensor sensor) {
		if (sensor.column != null && !sensor.column.isEmpty()) {
			return sensor.column;
		}

		int family_res = -1;
		switch(sensor.type) {
		case Database.I2c.ADXL345:
			family_res = R.string.adxl345;
		break;
		case Database.I2c.BMP180:
			family_res = R.string.bmp180;
		break;
		case Database.I2c.TMP006:
			family_res = R.string.tmp006;
		break;
		case Database.I2c.TSL2591:
			family_res = R.string.tsl2591;
		break;
		case Database.I2c.LM75:
			family_res = R.string.lm75;
		break;
		case Database.I2c.L3GD20:
			family_res = R.string.l3gd20;
		break;
		case Database.I2c.SI114X:
			family_res = R.string.si114x;
		break;
		default:
		return "";
		}

		String family = m_context.getString(family_res);
		return String.format(m_unknown_name_format, family, sensor.address);
	}

	private final ScheduledExecutorService scheduledExecutorService =
		Executors.newSingleThreadScheduledExecutor();
	private ScheduledFuture<?> m_futureRun;
	long m_start_time;
	boolean m_infinite;
	public int start(int time, int sample_rate) {
		if (m_entries.size() == 0) {
			return 0;
		}

		int total = time * sample_rate;
		for (int i = 0; i < m_entries.size(); i++) {
			Entry entry = m_entries.get(i);
			entry.sample_total = total;
			entry.sample_got = 0;
		}

		/* log for infinite */
		m_infinite = !(time > 0);

		int period /* ms */ = 1000 / sample_rate;
		m_start_time = System.currentTimeMillis();
		m_futureRun = scheduledExecutorService.scheduleAtFixedRate(
			this, 0, period, TimeUnit.MILLISECONDS);
		return 1;
	}

	public int stop() {
		if (m_futureRun == null) {
			return -1;
		}

		return m_futureRun.cancel(true) ?
			1 : -1;
	}

	public void run() {
		int entries_skipped = 0;
		for (int i = 0; i < m_entries.size(); i++) {
			Entry entry = m_entries.get(i);

			/* task has already been cancelled */
			if (m_futureRun.isCancelled()) {
				return;
			}

			/* logging stop condition */
			if (m_infinite == false) {
				if (entry.sample_got >= entry.sample_total) {
					entries_skipped++;
					continue;
				}
			}

			long curr_time = System.currentTimeMillis();

			boolean gotError;
			try {
				gotError = logEntry(entry);
			} catch(ResultException e) {
				if (Config.ERROR) Log.e(TAG, "unable to fetch data: " + e);
				gotError = true;
			}

			if (gotError) {
				entry.sample_total--;
			} else {
				double value = (curr_time - m_start_time) / 1000.0;
				Column.setValue(entry.sample_got, entry.col_time, value);
				entry.sample_got++;
			}
		}

		if (entries_skipped == m_entries.size()) {
			if (m_futureRun != null) {
				if (!m_futureRun.cancel(true)) {
					if (Config.ERROR) Log.e(TAG, "unable to stop i2c logging");
				}

				m_futureRun = null;
			}
		} else {
			/* manual change event called. to reduce the number of updates */
			Column.changed();
			Curve.changed();
		}
	}

	private boolean logEntry(Entry entry) throws ResultException {
		boolean gotError = false;

		switch(entry.type) {
		case Database.I2c.ADXL345: {
			Adxl345 driver = (Adxl345) entry.driver;
			double[] values = driver.read();
			double x = values[0] * ACCELERATION_DUE_TO_GRAVITY;
			double y = values[1] * ACCELERATION_DUE_TO_GRAVITY;
			double z = values[2] * ACCELERATION_DUE_TO_GRAVITY;
			Column.setValue(entry.sample_got, entry.cols_value[0], x);
			Column.setValue(entry.sample_got, entry.cols_value[1], y);
			Column.setValue(entry.sample_got, entry.cols_value[2], z);
		} break;
		case Database.I2c.BMP180: {
			Bmp180 driver = (Bmp180) entry.driver;
			double value = driver.read();
			Column.setValue(entry.sample_got, entry.cols_value[0], value);
		} break;
		case Database.I2c.TMP006: {
			Tmp006 driver = (Tmp006) entry.driver;
			double value = driver.read();
			Column.setValue(entry.sample_got, entry.cols_value[0], value);
		} break;
		case Database.I2c.TSL2591: {
			Tsl2591 driver = (Tsl2591) entry.driver;
			double value = driver.read();
			Column.setValue(entry.sample_got, entry.cols_value[0], value);
		} break;
		case Database.I2c.LM75: {
			Lm75 driver = (Lm75) entry.driver;
			double value = Extra.kelvinToCelsius(driver.read16());
			Column.setValue(entry.sample_got, entry.cols_value[0], value);
		} break;
		case Database.I2c.L3GD20: {
			L3gd20 driver = (L3gd20) entry.driver;
			double[] values = driver.read();
			double x = Extra.radianToDegree(values[0]);
			double y = Extra.radianToDegree(values[1]);
			double z = Extra.radianToDegree(values[2]);
			Column.setValue(entry.sample_got, entry.cols_value[0], x);
			Column.setValue(entry.sample_got, entry.cols_value[1], y);
			Column.setValue(entry.sample_got, entry.cols_value[2], z);
		} break;
		case Database.I2c.SI114X: {
			Si114x driver = (Si114x) entry.driver;
			double value = driver.readUvIndex();
			Column.setValue(entry.sample_got, entry.cols_value[0], value);
		} break;
		default: {
			if (Config.WTF) Log.wtf(TAG, "spi entry.type of of range");
			gotError = true;
		}
		}

		return gotError;
	}
}
