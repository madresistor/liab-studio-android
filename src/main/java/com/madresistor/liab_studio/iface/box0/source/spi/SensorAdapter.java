/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.iface.box0.source.spi;

import android.content.Context;

import com.madresistor.box0.module.Spi;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.iface.box0.source.Database;
import com.madresistor.liab_studio.iface.box0.source.common.SensorBaseAdapter;
import com.madresistor.liab_studio.iface.box0.source.spi.EditSensorDialog;

import java.util.ArrayList;
import java.util.List;

public class SensorAdapter extends SensorBaseAdapter {
	List<Sensor> m_sensors = new ArrayList<Sensor>();
	private int m_count;
	private final String m_fmt_unnamed;

	public SensorAdapter(Context context) {
		super(context);
		m_fmt_unnamed = context.getString(
			R.string.iface_box0_spi_unnamed_channel);
	}

	public int getCount() {
		return m_count;
	}

	public void setSelected(int index, boolean selected) {
		m_sensors.get(index).selected = selected;
	}

	public boolean isSelected(int index) {
		return m_sensors.get(index).selected;
	}

	public String getTextTop(int index) {
		String v = m_sensors.get(index).channel;
		if (v == null) {
			v = "";
		}
		return v;
	}

	public String getTextRight(int index) {
		String v = m_sensors.get(index).column;
		if (v == null) {
			v = getTextTop(index);
		}
		return v;
	}

	public String getTextLeft(int index) {
		int type = m_sensors.get(index).type;
		return Database.spi.get(type).name;
	}

	/* this is called when user want to edit sensor */
	public void editSensor(int index) {
		Sensor sensor = m_sensors.get(index);
		EditSensorDialog dialog = new EditSensorDialog(
			getContext(), this, sensor);
		dialog.show();
	}

	public void setModule(Spi spi) {
		if (spi == null) {
			return;
		}

		m_count = spi.ssCount;
		for (int i = 0; i < m_count; i++) {
			String value = spi.label.ss[i];

			if (value == null) {
				value = String.format(m_fmt_unnamed, i);
			}

			if (m_sensors.size() <= i) {
				m_sensors.add(new Sensor());
			}

			Sensor sensor = m_sensors.get(i);
			sensor.channel = value;
		}

		notifyDataSetChanged();
	}

	public Sensor getSensor(int position) {
		return m_sensors.get(position);
	}

	public static class Sensor {
		boolean selected;
		String channel;
		int type;
		String column;
	}
}
