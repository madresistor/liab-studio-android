/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.ui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

import com.madresistor.liab_studio.ui.colorpicker.ColorPickerDialog;

public class EditBackgroundColorOnClick implements View.OnClickListener {
	private static EditBackgroundColorOnClick m_instance = null;
	public static View.OnClickListener getInstance() {
		if (m_instance == null) {
			m_instance = new EditBackgroundColorOnClick();
		}

		return m_instance;
	}

	@Override
	public void onClick(final View view) {
		ColorDrawable drawable = (ColorDrawable) view.getBackground();
		int color = drawable.getColor();
		Context context = view.getContext();
		ColorPickerDialog.OnColorSelectedListener listener =
			new ColorPickerDialog.OnColorSelectedListener() {
				public void onColorSelected(int color) {
					view.setBackgroundColor(color);
				}
			};

		ColorPickerDialog colorPicker =
			new ColorPickerDialog(context, color, listener);
		colorPicker.show();
	}
}
