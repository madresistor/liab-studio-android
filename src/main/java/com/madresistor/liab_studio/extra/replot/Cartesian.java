/*
 * This file is part of android-replot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * android-replot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-replot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-replot.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.extra.replot;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.WindowManager;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.IndexOutOfBoundsException;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Cartesian Plot based on OpenGL and libreplot.
 * This class provide the direct interface to libreplot Plot and Arg.
 * Along with the JNI, you can customize it for your purpose.
 */
public abstract class Cartesian extends GLSurfaceView implements GLSurfaceView.Renderer {
	private ScaleGestureDetector m_scaleDetector;
	private GestureDetector m_gestureDetector;
	private TranslateGestureDetector m_translateDetector;

	protected ByteBuffer /* (replot_cartesian*) */ m_native = null;

	public Cartesian(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public Cartesian(Context context) {
		super(context);
		init(context);
	}

	/**
	 * perform int of the GLSurfaceView
	 * @param context Activity context
	 * @see readAssetFile()
	 */
	private void init(Context context) {

		/* Create an OpenGL ES 2.0 context */
		setEGLContextClientVersion(2);

		/*  make transparent dont-work? */
		setEGLConfigChooser(8, 8, 8, 8, 16, 0);

		/* Set the Renderer for drawing on the GLSurfaceView
		 * methods are implemented in JNI
		 */
		setRenderer(this);

		/*
		 * plot will only be rendered when user perform some translation or scale operation
		 * or replot call is performed from JNI
		 */
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		getHolder().setFormat(PixelFormat.TRANSLUCENT);

		/* scale detector */
		m_scaleDetector = new ScaleGestureDetector(context, new ScaleListener());

		/* gesture detector
		 * this is used for detect double click
		 * when user double-click => all translation and scale are reset
		 */
		m_gestureDetector = new GestureDetector(context, new GestureListener());

		/* scale detector */
		m_translateDetector = new TranslateGestureDetector(context, new TranslateListener());
	}

	/**
	 * Get the native handle (replot_cartesian *)
	 * Be careful with that. :)
	 * @return pointer of native handle
	 */
	public ByteBuffer getNative() {
		return m_native;
	}

	/**
	 * Set native handle.
	 */
	public void setNative(ByteBuffer /* (replot_cartesian *) */ value) {
		m_native = value;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		boolean a, b, c;
		a = m_scaleDetector.onTouchEvent(ev);
		b = m_gestureDetector.onTouchEvent(ev);
		c = m_translateDetector.onTouchEvent(ev);
		return a || b || c;
	}

	/**
	 * Translation listener.
	 *  This will route the translation callback to Plot methods.
	 */
	private class TranslateListener implements TranslateGestureDetector.OnTranslateListener {
		public void onStart(float x, float y) {
			onTranslationStart(x, y);
		}

		public void onPerform(float dx, float dy) {
			onTranslationRelative(dx, dy);
		}

		public void onStop(float x, float y) {
			onTranslationStop(x, y);
		}

		public boolean isScaleInProgress() {
			return m_scaleDetector.isInProgress();
		}
	}

	/**
	 * Scaling listener.
	 *  This will route the scaling callback to Plot methods.
	 */
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			float dx = detector.getCurrentSpanX() / detector.getPreviousSpanX();
			float dy = detector.getCurrentSpanY() / detector.getPreviousSpanY();
			onScaleRelative(dx, dy);
			return true;
		}
	}

	/**
	 * Gesture listener.
	 *  This will route the gestures callback to Plot methods.
	 */
	private class GestureListener extends GestureDetector.SimpleOnGestureListener {
		/* double tap to move to min = 0 and max = 1 */
		@Override
		public boolean onDoubleTapEvent(MotionEvent e) {
			Cartesian.this.onDoubleTap(e.getX(), e.getY());
			return true;
		}

		/* Ref: http://stackoverflow.com/questions/19729007 */
		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		/* dpi */
		WindowManager windowManager = (WindowManager)
			getContext().getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(metrics);
		float xdpi = metrics.xdpi;
		float ydpi = metrics.ydpi;

		m_native = onSurfaceChanged(m_native, width, height, xdpi, ydpi);
	}

	/**
	 * onDrawFrame is the method that does the actual plotting.
	 * subclass need to initalize curves list before calling this method.
	 * Currently there is no method to build curves list with JNI.
	 */
	@Override
	public void onDrawFrame(GL10 gl) {
		onDrawFrame(m_native);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		onSurfaceCreated(m_native);
	}

	/**
	 * Called when user start moving the plot with finger.
	 * @param x X coordinate in Pixel
	 * @param y Y coordinate in Pixel
	 */
	public void onTranslationStart(float x, float y) {
		onTranslationStart(getNative(), x, y);
		requestRender();
	}

	/**
	 * Called when user actually start moving the plot
	 * @param dx The number of pixels change in X coordinate
	 * @param dy The number of pixels change in Y coordinate
	 */
	public void onTranslationRelative(float dx, float dy) {
		onTranslationRelative(getNative(), dx, dy);
		requestRender();
	}

	/**
	 * Called when user lift up finger.
	 * @param x X coordinate in pixel
	 * @param y Y coordinate in pixel
	 */
	public void onTranslationStop(float x, float y) {
		onTranslationStop(getNative());
		requestRender();
	}

	/**
	 * When user perform scale of plot via 2 fingers.
	 * @param dx Ratiometric change in X axis
	 * @param dy Ratiometric change in Y axis
	 */
	public void onScaleRelative(float dx, float dy) {
		onScaleRelative(getNative(), dx, dy);
		requestRender();
	}

	/**
	 * called when user double tap.
	 * The intutive double tap.
	 * the main aim to have this in UI so that, double tap can reset to best fit.
	 * @param x X coordinate
	 * @param y Y coordinate
	 */
	public void onDoubleTap(float x, float y) {
		onDoubleTap(getNative());
		requestRender();
	}

	private native static void onSurfaceCreated(
		ByteBuffer /* (android_plot*) */m_native);

	/* renderer method */
	private native static ByteBuffer /* (android_plot*) */ onSurfaceChanged(
			ByteBuffer /* (android_plot*) */ arg, int width, int height,
			float xdpi, float ydpi);

	private native static void onDrawFrame(
			ByteBuffer /* (android_plot*) */ arg);

	/* ui methods */
	private native static void onTranslationStart(
			ByteBuffer /* (android_plot*) */ arg, float x, float y);

	private native static void onTranslationRelative(
			ByteBuffer /* (android_plot*) */ arg, float dx, float dy);

	private native static void onTranslationStop(
			ByteBuffer /* (android_plot*) */ arg);

	private native static void onScaleRelative(
			ByteBuffer /* (android_plot*) */ arg, float dx, float dy);

	private native static void onDoubleTap(
			ByteBuffer /* (android_plot*) */ arg);

	private native static void freeNativeMemory(
			ByteBuffer /* (android_plot*) */ arg);

	@Override
	protected void finalize() throws Throwable {

		if (m_native != null) {
			freeNativeMemory(m_native);
		}

		super.finalize();
	}

	static {
		System.loadLibrary("z");

		System.loadLibrary("png"); /* z */

		System.loadLibrary("freetype2"); /* z */

		System.loadLibrary("harfbuzz"); /* freetype2 */

		System.loadLibrary("replot"); /* harfbuzz, png, freetype2 */
	}
}
