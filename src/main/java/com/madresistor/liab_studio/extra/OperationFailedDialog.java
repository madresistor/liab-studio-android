/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.extra;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.madresistor.liab_studio.R;

/**
 * Used to show CSV export/import operation has failed.
 */
public class OperationFailedDialog extends AlertDialog
		implements DialogInterface.OnClickListener {
	public OperationFailedDialog(Context context, Exception e) {
		/* TODO: make this message more enduser friendly. */
		this(context, e.getMessage());
	}

	public OperationFailedDialog(Context context, String msg) {
		super(context);

		setIcon(R.drawable.ic_alert_circle_grey600_48dp);
		setTitle(context.getString(R.string.failed));
		setMessage(msg);

		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.ok), this);
	}

	public void onClick(DialogInterface dialog, int which) {}
}
