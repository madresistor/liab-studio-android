/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.widget.graph;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.opengl.GLES20;

import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.JniLoader;
import com.madresistor.liab_studio.extra.replot.Cartesian;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.io.IOException;
import java.lang.IndexOutOfBoundsException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.Semaphore;

public class PlotView extends Cartesian {
	public PlotView(Context context, AttributeSet attr) {
		super(context, attr);
	}

	private CaptureBitmap capture_at_end = null;

	@Override
	public void onDrawFrame(GL10 gl) {
		onDrawFrame(getNative());

		if (capture_at_end != null) {
			capture_at_end.onDrawFrame(gl);
			capture_at_end = null;
		}
	}

	/*
	 * Hybrid of:
	 *
	 *  Project: https://github.com/google/grafika
	 *  Source: src/com/android/grafika/gles/EglSurfaceBase.java
	 *  Licence: Apache Licence 2.0
	 *
	 *  Project: https://github.com/CyberAgent/android-gpuimage
	 *  Source: library/src/jp/co/cyberagent/android/gpuimage/GPUImageView.java
	 *  Licence: Apache Licence 2.0
	 */
	private class CaptureBitmap {
		private final Semaphore waiter = new Semaphore(0);
		private final int width, height;
		private final ByteBuffer pixelBuffer;

		public CaptureBitmap() {
			width = getMeasuredWidth();
			height = getMeasuredHeight();

			// glReadPixels fills in a "direct" ByteBuffer with what is essentially big-endian RGBA
			// data (i.e. a byte of red, followed by a byte of green...).  While the Bitmap
			// constructor that takes an int[] wants little-endian ARGB (blue/red swapped), the
			// Bitmap "copy pixels" method wants the same format GL provides.
			//
			// Ideally we'd have some way to re-use the ByteBuffer, especially if we're calling
			// here often.
			//
			// Making this even more interesting is the upside-down nature of GL, which means
			// our output will look upside down relative to what appears on screen if the
			// typical GL conventions are used.
			pixelBuffer = ByteBuffer.allocate(width * height * 4);
			pixelBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		/**
		 * Get a bitmap of the surface
		 * @throws InterruptedException
		 */
		public Bitmap getBitmap() throws InterruptedException {
			capture_at_end = this;
			requestRender();
			waiter.acquire();

			/* Convert upside down mirror-reversed
			 *   image to right-side up normal image. */
			byte[] pixelArray = pixelBuffer.array();
			int len = width * 4;
			byte[] tmp = new byte[len];
			for (int i = 0; i < height / 2; i++) {
				int top_index = i * len;
				int bot_index = (height - i - 1) * len;
				System.arraycopy(pixelArray, top_index, tmp, 0, len);
				System.arraycopy(pixelArray, bot_index, pixelArray, top_index, len);
				System.arraycopy(tmp, 0, pixelArray, bot_index, len);
			}

			pixelBuffer.rewind();
			Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			bmp.copyPixelsFromBuffer(pixelBuffer);
			return bmp;
		}

		/**
		 * in the GL thread, this method will read the pixel of the surface
		 *   and store them to pixelBuffer and
		 *  release waiter so, that getBitmap can proceed further.
		 */
		public void onDrawFrame(GL10 gl) {
			gl.glReadPixels(0, 0, width, height, GLES20.GL_RGBA,
					GLES20.GL_UNSIGNED_BYTE, pixelBuffer);
			waiter.release();
		}
	}

	/**
	 * Get a Bitmap of the surface.
	 *  getDrawingCache will fail to return the correct bitmap because
	 *   it do not work for GLSurfaceView.
	 * This will block the thread until the GL Thread redraw the plot.
	 */
	public Bitmap getBitmap() throws InterruptedException {
		return new CaptureBitmap().getBitmap();
	}

	private native void onDrawFrame(ByteBuffer /* (replot_cartesian *) */ arg);

	static {
		JniLoader.glue();
	}
}
