/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.widget.graph;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.tunnel.Curve;
import com.madresistor.liab_studio.tunnel.CurveBaseAdapter;
import com.madresistor.liab_studio.widget.graph.curve.AddDialog;
import com.madresistor.liab_studio.widget.graph.curve.EditDialog;

/*
 * List all curves.
 * 	click: show/hide curves
 *  long-click: edit curve
 */
public class ListDialog extends AlertDialog
					implements DialogInterface.OnClickListener {
	private final Context m_context;
	public ListDialog(Context context) {
		super(context);
		m_context = context;
		if (Curve.getCurveCount() > 0) {
			loadListView();
		} else {
			loadEmptyView();
		}
	}

	private void loadEmptyView() {
		LayoutInflater infater = LayoutInflater.from(m_context);
		View rootView = infater.inflate(
			R.layout.large_empty_text, null);

		/* add facility */
		setButton(DialogInterface.BUTTON_POSITIVE,
				m_context.getString(R.string.add), this);

		setView(rootView);
	}

	private void loadListView() {
		ListView listView = new ListView(m_context);
		CurveAdapter adapter = new CurveAdapter(m_context);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(adapter);
		listView.setOnItemLongClickListener(adapter);
		setView(listView);
	}

	private static class CurveAdapter extends CurveBaseAdapter
				implements AdapterView.OnItemClickListener,
							AdapterView.OnItemLongClickListener {
		private final LayoutInflater m_inflater;
		private final Context m_context;
		public CurveAdapter(Context context) {
			m_context = context;
			m_inflater = (LayoutInflater) context.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(
					android.R.layout.simple_list_item_multiple_choice , parent, false);
			}

			CheckedTextView checkedTextView = (CheckedTextView) convertView;
			checkedTextView.setText(Curve.getName(position));
			checkedTextView.setChecked(Curve.isVisible(position));
			checkedTextView.setTextColor(Curve.getColor(position));
			return checkedTextView;
		}

		@Override
		public int getCount() {
			return Curve.getCurveCount();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void onItemClick(AdapterView<?> parent, View view,
			int position, long id) {
			CheckedTextView checkedTextView = (CheckedTextView) view;
			checkedTextView.toggle(); /* JUST WORKS! */
			boolean visible = checkedTextView.isChecked();
			Curve.setVisible(position, visible);
		}

		public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
			EditDialog dialog = new EditDialog(m_context, position);
			dialog.show();
			return true;
		}
	}

	public void onClick(DialogInterface _dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			AddDialog dialog = new AddDialog(m_context);
			dialog.show();
		}

		ListDialog.this.dismiss();
	}
}
