/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.widget.graph;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.madresistor.liab_studio.widget.graph.PlotView;
import com.madresistor.liab_studio.extra.OperationThread;

/**
 * Export the PlotView to a PNG file.
 * This will extract a Bitmap from the PlotView and save it to a PNG file
 * no need to call start(), the constructor does it for you.
 */
public class PngExporter extends OperationThread {
	private final PlotView m_plotView;
	private final File m_file;

	public PngExporter(PlotView plot, File file) {
		super("PNG Exporter");
		m_plotView = plot;
		m_file = file;
		start();
	}

	public void run() {
		try {
			Bitmap bmp = m_plotView.getBitmap();

			FileOutputStream fos = new FileOutputStream(m_file);
			bmp.compress(Bitmap.CompressFormat.PNG, 95, fos);
			fos.close();
			bmp.recycle();
			successToast(m_plotView.getContext(), m_file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
			failedDialog(m_plotView.getContext(), e);
		}
	}
}
