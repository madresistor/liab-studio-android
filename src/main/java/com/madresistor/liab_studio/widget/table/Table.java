/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.widget.table;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.util.Log;
import android.os.Environment;
import android.content.Intent;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.Config;
import com.madresistor.liab_studio.SharedInfo;
import com.madresistor.liab_studio.widget.Widget;
import com.madresistor.liab_studio.widget.table.TableAdapter;
import com.madresistor.liab_studio.widget.table.column.AddDialog;
import com.madresistor.liab_studio.widget.table.column.ClearAllDialog;
import com.madresistor.liab_studio.widget.table.column.RemoveAllDialog;
import com.madresistor.liab_studio.extra.filepicker.ExtFilteredFilePickerActivity;
import com.madresistor.liab_studio.tunnel.Column;
import com.madresistor.liab_studio.widget.table.csv.Exporter;
import com.madresistor.liab_studio.widget.table.csv.Importer;

public class Table extends Widget {
	private final static String TAG = Table.class.getName();
	public Table(Context context) {
		super(context);
	}

	private final Frontend m_frontend = new Frontend();
	public Frontend getFrontend() {
		return m_frontend;
	}

	private void exportCsv(File file) {
		new Exporter(getContext(), file);
	}

	private void exportCsv() {
		/* verify that we have data */
		if (Column.getColumnCount() <= 0 || Column.getRowCount() <= 0) {
			if (Config.DEBUG) Log.d(TAG, "no data to save");
			return;
		}

		/* try to build a unique filename */
		String filename = getFileNameUsingTitle() + ".csv";

		/* do it! */
		exportCsv(new File(getDocumentsDirectory(), filename));
	}

	private void importCsv() {
		Intent i = new Intent(Intent.ACTION_GET_CONTENT);
		i.putExtra(ExtFilteredFilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
		i.putExtra(ExtFilteredFilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
		i.putExtra(ExtFilteredFilePickerActivity.EXTRA_MODE, ExtFilteredFilePickerActivity.MODE_FILE);
		i.putExtra(ExtFilteredFilePickerActivity.EXTRA_START_PATH, getDocumentsDirectory().getPath());
		i.putExtra(ExtFilteredFilePickerActivity.EXTRA_ALLOWED_EXT, "csv");

		m_frontend.startActivityForResult(i, SharedInfo.REQ_CODE_TABLE_CSV);
	}

	/**
	 * When user import CSV file, older data will be cleared.
	 *  This dialog is used to get context.
	 */
	private class ImportClearEverythingConcentDialog extends RemoveAllDialog {
		private final File m_file;
		public ImportClearEverythingConcentDialog(File file) {
			super(Table.this.getContext());
			m_file = file;
		}

		/**
		 * pass the control to importCsv() with warnMe=false
		 */
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				importCsv(m_file, false);
			}
		}
	}

	/**
	 * Import CSV file.
	 * @param file File
	 * @param warnMe warn user that this operation will clear all previous data
	 */
	private void importCsv(File file, boolean warnMe) {
		/* check if do we indeed have some useful data. */
		if (warnMe) {
			warnMe = Column.hasUsefulData();
		}

		/* warn user that data will be cleared when importing CSV file */
		if (warnMe) {
			ImportClearEverythingConcentDialog dialog  =
				new ImportClearEverythingConcentDialog(file);
			dialog.show();
			return;
		}

		new Importer(getContext(), file);
	}

	private void importCsv(File file) {
		importCsv(file, true);
	}

	@Override
	public boolean consumeIndent(Intent intent) {
		if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			String filepath = intent.getData().getPath();
			importCsv(new File(filepath));
			return true;
		}

		return false;
	}

	private class Frontend extends Widget.Frontend {
		TableFixHeaders m_bind;

		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.widget_table, container, false);
			m_bind = (TableFixHeaders) rootView.findViewById(R.id.widget_table_bind);
			prepareTable();
			return rootView;
		}

		@Override
		public void onDestroyView() {
			m_bind = null;
			super.onDestroyView();
		}

		void prepareTable() {
			m_bind.setAdapter(new TableAdapter(getActivity()));
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch(item.getItemId()) {
			case R.id.widget_table_add: {
				AddDialog dialog = new AddDialog(getActivity());
				dialog.show();
			} return true;
			case R.id.widget_table_all_column_clear: {
				ClearAllDialog dialog = new ClearAllDialog(getActivity());
				dialog.show();
			} return true;
			case R.id.widget_table_all_column_remove: {
				RemoveAllDialog dialog = new RemoveAllDialog(getActivity());
				dialog.show();
			} return true;
			case R.id.widget_table_export_csv: {
				exportCsv();
			} return true;
			case R.id.widget_table_import_csv: {
				importCsv();
			} return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.widget_table_actions, menu);
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (resultCode != Activity.RESULT_OK) {
				return;
			}

			if (requestCode == SharedInfo.REQ_CODE_TABLE_CSV) {
				String filepath = data.getData().getPath();
				importCsv(new File(filepath));
			}
		}
	}

	static {
		Widget.register(Table.class);
	}
}
