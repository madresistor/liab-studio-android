/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio.widget.recent;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.madresistor.liab_studio.R;
import com.madresistor.liab_studio.tunnel.Column;
import com.madresistor.liab_studio.widget.Widget;
import com.madresistor.liab_studio.widget.recent.RecentView;
import com.madresistor.liab_studio.widget.recent.column.ListDialog;

public class Recent extends Widget implements Column.Observer {
	public Recent(Context context) {
		super(context);
		Column.registerObserver(this);
	}

	private final Frontend m_frontend = new Frontend();
	public Frontend getFrontend() {
		return m_frontend;
	}

	public void onColumnChanged() {
		m_frontend.update();
	}

	public class Frontend extends Widget.Frontend {
		private RecentView m_view = null;

		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.widget_recent, container, false);
			m_view = (RecentView) rootView.findViewById(R.id.widget_recent_view);
			update();
			return rootView;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch(item.getItemId()) {
			case R.id.widget_recent_visible:
				ListDialog dialog = new ListDialog(getActivity());
				dialog.show();
			return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.widget_recent_actions, menu);
		}

		@Override
		public void onDestroyView() {
			m_view = null;
			super.onDestroyView();
		}

		public void update() {
			if (m_view != null) {
				m_view.update();
			}
		}
	}

	static {
		Widget.register(Recent.class);
	}
}
