/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.liab_studio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.madresistor.liab_studio.Module;
import com.madresistor.liab_studio.extra.AboutFragment;
import com.madresistor.liab_studio.iface.box0.Box0;
import com.madresistor.liab_studio.ui.IfaceProgress;
import com.madresistor.liab_studio.widget.graph.Graph;
import com.madresistor.liab_studio.widget.recent.Recent;
import com.madresistor.liab_studio.widget.table.Table;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class MainActivity extends AppCompatActivity
	implements ModuleSelector.OnModuleSelectedListener {
	private final static String TAG = MainActivity.class.getName();

	private Module[] m_modules;

	/* TAG used for saving and load current module from bundle */
	private static final String CURRENT_MODULE = TAG + ".CURRENT_MODULE";
	private static final int DEFAULT_MODULE = ModuleSelector.OnModuleSelectedListener.GRAPH;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (Config.DEVELOPER_MODE) {
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads()
				.detectDiskWrites()
				.detectAll()
				.penaltyLog()
				.build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
				.detectLeakedSqlLiteObjects()
				.detectLeakedClosableObjects()
				.penaltyLog()
				.penaltyDeath()
				.build());
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		Toolbar toolbar = (Toolbar) findViewById(R.id.main_activity_toolbar);
		toolbar.setLogo(R.drawable.ic_launcher);
		toolbar.setLogoDescription(R.string.app_name);
		//~ toolbar.setTitle(null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle(null);

		/* modules */

		IfaceProgress ifaceProgress = (IfaceProgress) findViewById(R.id.main_activity_progress_bar);

		Box0 box0 = new Box0(this);
		box0.setOnStatusChangedListener(ifaceProgress);

		m_modules = new Module [] {
			new Graph(this),
			new Table(this),
			new Recent(this),
			box0
		};

		/* ==== Load inital fragment ==== */
		int index = (savedInstanceState != null) ?
			savedInstanceState.getInt(CURRENT_MODULE, DEFAULT_MODULE) :
			DEFAULT_MODULE;

		loadModule(index, savedInstanceState == null);
	}

	/* Ref: http://stackoverflow.com/questions/17828869 */
	@Override
	protected void onResume() {
		super.onResume();

		Intent intent = getIntent();
		if (intent != null) {
			distributeIntent(intent);
		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		distributeIntent(intent);
	}

	private void distributeIntent(Intent intent) {
		/* route the intent to modules,
		 *  anyone interested can utilize it */
		int i = 0;
		for (Module m: m_modules) {
			if (m.consumeIndent(intent)) {
				loadModule(i);
			}
			i++;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity_actions, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		switch(item.getItemId()) {
		case R.id.main_activity_module_selector: {
			ModuleSelector dialog = new ModuleSelector(this, this);
			dialog.show();
		} return true;
		case R.id.main_activity_about: {
			Fragment fragment = new AboutFragment();
			FragmentManager fm = getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			ft.replace(R.id.main_activity_widget, fragment);
			ft.addToBackStack(null);
			ft.commit();
		} return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putInt(CURRENT_MODULE, m_currentModuleIndex);
	}

	public void loadModule(int index) {
		loadModule(index, false);
	}

	private int m_currentModuleIndex;
	public void loadModule(int index, boolean firstTime) {
		if (index >= 0 && index <= m_modules.length) {
			FragmentManager fm = getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

			Fragment fragment = m_modules[index].getFrontend();
			if (firstTime) {
				ft.add(R.id.main_activity_widget, fragment);
			} else {
				ft.replace(R.id.main_activity_widget, fragment);
				if (index != m_currentModuleIndex) {
					ft.addToBackStack(null);
				}
			}

			// prevent crash in some senerio,
			//  like when app in sleep but need to change fragment.
			// seen when, device connected and switch fragment (after onSaveInstanceState)
			//  "Can not perform this action after onSaveInstanceState" crash reason
			ft.commitAllowingStateLoss();

			m_currentModuleIndex = index;
		} else {
			throw new IllegalArgumentException("index is unknown: " + index);
		}
	}

	public void onModuleSelected(int type, int sub_type) {
		loadModule(type * 3 + sub_type);
	}

	private Module instantiate(Class<Module> cls) {
		Class[] paramsType = { Context.class };
		Constructor cons;

		try {
			cons = cls.getConstructor(paramsType);
		} catch(NoSuchMethodException e) {
			if (Config.WTF) Log.wtf(TAG, "getConstructor failed for " + cls + " with exception " + e);
			/* show internal error occured message to user */
			return null;
		}

		Object[] paramsValue = { this };
		Module module;

		try {
			module = (Module) cons.newInstance(paramsValue);
		} catch(InstantiationException e) {
			if (Config.WTF) Log.wtf(TAG, "newInstance failed for " + cls + " with exception " + e);
			/* show internal error occured message to user */
			return null;
		} catch(IllegalAccessException e) {
			if (Config.WTF) Log.wtf(TAG, "newInstance failed for " + cls + " with exception " + e);
			/* show internal error occured message to user */
			return null;
		} catch(InvocationTargetException e) {
			if (Config.WTF) Log.wtf(TAG, "newInstance failed for " + cls + " with exception " + e);
			/* show internal error occured message to user */
			return null;
		}

		return module;
	}
}
